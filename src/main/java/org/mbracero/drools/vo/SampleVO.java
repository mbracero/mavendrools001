package org.mbracero.drools.vo;

public class SampleVO {
	private String stringValue = "";
	private boolean booleanValue = true;
	public String getStringValue() {
		return stringValue;
	}
	public void setStringValue(String stringValue) {
		this.stringValue = stringValue;
	}
	public boolean isBooleanValue() {
		return booleanValue;
	}
	public void setBooleanValue(boolean booleanValue) {
		this.booleanValue = booleanValue;
	}
	@Override
	public String toString() {
		return "SampleVO [stringValue=" + stringValue + ", booleanValue="
				+ booleanValue + "]";
	}
}
