package org.mbracero.drools.vo;

public class Item {
	private int id;
	private String stringValue = "";
	private boolean valid = false;
	
	public Item() {}
	public Item(int id, String stringValue, boolean valid) {
		this.id = id;
		this.stringValue = stringValue;
		this.valid = valid;
	}
	public Item(String stringValue, boolean valid) {
		this.stringValue = stringValue;
		this.valid = valid;
	}
	
	public String getStringValue() {
		return stringValue;
	}
	public void setStringValue(String stringValue) {
		this.stringValue = stringValue;
	}
	public boolean isValid() {
		return valid;
	}
	public void setValid(boolean valid) {
		this.valid = valid;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "Item [id=" + id + ", stringValue=" + stringValue
				+ ", valid=" + valid + "]";
	}
}
