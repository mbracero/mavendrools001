package org.mbracero.drools.config;

import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderError;
import org.drools.builder.KnowledgeBuilderErrors;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.io.ResourceFactory;
import org.drools.runtime.StatefulKnowledgeSession;
import org.drools.runtime.StatelessKnowledgeSession;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class RulesConfig {
	
	/**
	 * CON STANDARD JAVA SINTAX
	 * ------------------------
	 * 	Para esta regla se aplican cambios internos en los objetos, sin embargo hasta que no terminan de ejecutarse
	 * todas las reglas no se ven reflejados los cambios en dichos objectos (aunque se muestre en el fichero que realmente
	 * se han cambiado los atributos del objeto).
	 * 
	 * 	El resultado que ofrece cada uno de las reglas es como se ejecutase independientemente de los demas.
	 * 
	 * CON INFERENCIA
	 * --------------
	 * 	Se aplican los cambios internamente durante la ejecución de las reglas (no ocurre lo de antes).
	 * 
	 * 	Da igual que sea con (stateful) o sin estado (stateless).
	 * 
	 * 	Ver DroolRule01Test.
	 */
	@Value("${rule01.drl}")
	private String rule01;
	
	@Bean(name="rule01Stateless")
	public StatelessKnowledgeSession configureKBaseStateless() {
		KnowledgeBuilder kbuilder = KnowledgeBuilderFactory
				.newKnowledgeBuilder();
		kbuilder.add(ResourceFactory.newClassPathResource(rule01),
				ResourceType.DRL);
		KnowledgeBuilderErrors errors = kbuilder.getErrors();
		if (errors.size() > 0) {
			for (KnowledgeBuilderError error : errors) {
				System.err.println(error);
			}
			throw new IllegalArgumentException("Could not parse knowledge.");
		}
		KnowledgeBase kbase = KnowledgeBaseFactory.newKnowledgeBase();
		kbase.addKnowledgePackages(kbuilder.getKnowledgePackages());
		
		StatelessKnowledgeSession ksession = kbase.newStatelessKnowledgeSession();
		
		return ksession;
	}
	
	@Bean(name="rule01Stateful")
	public StatefulKnowledgeSession configureKBaseStateful() {
		KnowledgeBuilder kbuilder = KnowledgeBuilderFactory
				.newKnowledgeBuilder();
		kbuilder.add(ResourceFactory.newClassPathResource(rule01),
				ResourceType.DRL);
		KnowledgeBuilderErrors errors = kbuilder.getErrors();
		if (errors.size() > 0) {
			for (KnowledgeBuilderError error : errors) {
				System.err.println(error);
			}
			throw new IllegalArgumentException("Could not parse knowledge.");
		}
		KnowledgeBase kbase = KnowledgeBaseFactory.newKnowledgeBase();
		kbase.addKnowledgePackages(kbuilder.getKnowledgePackages());
		
		StatefulKnowledgeSession ksession = kbase.newStatefulKnowledgeSession();
		
		return ksession;
	}
	
	
	
	
	
	/**
	 * 	En este caso hemos separado el fichero rule01 en dos partes:
	 * 		La primera en la que se modifican los objetos.
	 * 		La segunda en donde se comprueban si son validos o no.
	 * 
	 * 	En este caso si se obtiene un resultado esperado para cada objeto que se incluyen en las reglas.
	 * 
	 * 	Tanto para stateful como stateless (y con con standard java syntax o inferencia) se obtienen los mismos resultados.
	 * 
	 * 	Aqui solo se realiza modificaciones de objetos con standard java syntax.
	 * 
	 * 	Ver DroolRule02Test.
	 */
	@Value("${rule02a.drl}")
	private String rule02a;
	
	@Bean(name="rule02aStateless")
	public StatelessKnowledgeSession configureKBaseStateless02a() {
		KnowledgeBuilder kbuilder = KnowledgeBuilderFactory
				.newKnowledgeBuilder();
		kbuilder.add(ResourceFactory.newClassPathResource(rule02a),
				ResourceType.DRL);
		KnowledgeBuilderErrors errors = kbuilder.getErrors();
		if (errors.size() > 0) {
			for (KnowledgeBuilderError error : errors) {
				System.err.println(error);
			}
			throw new IllegalArgumentException("Could not parse knowledge.");
		}
		KnowledgeBase kbase = KnowledgeBaseFactory.newKnowledgeBase();
		kbase.addKnowledgePackages(kbuilder.getKnowledgePackages());
		
		StatelessKnowledgeSession ksession = kbase.newStatelessKnowledgeSession();
		
		return ksession;
	}
	
	@Bean(name="rule02aStateful")
	public StatefulKnowledgeSession configureKBaseStateful02a() {
		KnowledgeBuilder kbuilder = KnowledgeBuilderFactory
				.newKnowledgeBuilder();
		kbuilder.add(ResourceFactory.newClassPathResource(rule02a),
				ResourceType.DRL);
		KnowledgeBuilderErrors errors = kbuilder.getErrors();
		if (errors.size() > 0) {
			for (KnowledgeBuilderError error : errors) {
				System.err.println(error);
			}
			throw new IllegalArgumentException("Could not parse knowledge.");
		}
		KnowledgeBase kbase = KnowledgeBaseFactory.newKnowledgeBase();
		kbase.addKnowledgePackages(kbuilder.getKnowledgePackages());

		StatefulKnowledgeSession ksession = kbase.newStatefulKnowledgeSession();
		
		return ksession;
	}
	
	@Value("${rule02b.drl}")
	private String rule02b;
	
	@Bean(name="rule02bStateless")
	public StatelessKnowledgeSession configureKBaseStateless02b() {
		KnowledgeBuilder kbuilder = KnowledgeBuilderFactory
				.newKnowledgeBuilder();
		kbuilder.add(ResourceFactory.newClassPathResource(rule02b),
				ResourceType.DRL);
		KnowledgeBuilderErrors errors = kbuilder.getErrors();
		if (errors.size() > 0) {
			for (KnowledgeBuilderError error : errors) {
				System.err.println(error);
			}
			throw new IllegalArgumentException("Could not parse knowledge.");
		}
		KnowledgeBase kbase = KnowledgeBaseFactory.newKnowledgeBase();
		kbase.addKnowledgePackages(kbuilder.getKnowledgePackages());

		StatelessKnowledgeSession ksession = kbase.newStatelessKnowledgeSession();
		
		return ksession;
	}
	
	@Bean(name="rule02bStateful")
	public StatefulKnowledgeSession configureKBaseStateful02b() {
		KnowledgeBuilder kbuilder = KnowledgeBuilderFactory
				.newKnowledgeBuilder();
		kbuilder.add(ResourceFactory.newClassPathResource(rule02b),
				ResourceType.DRL);
		KnowledgeBuilderErrors errors = kbuilder.getErrors();
		if (errors.size() > 0) {
			for (KnowledgeBuilderError error : errors) {
				System.err.println(error);
			}
			throw new IllegalArgumentException("Could not parse knowledge.");
		}
		KnowledgeBase kbase = KnowledgeBaseFactory.newKnowledgeBase();
		kbase.addKnowledgePackages(kbuilder.getKnowledgePackages());

		StatefulKnowledgeSession ksession = kbase.newStatefulKnowledgeSession();
		
		return ksession;
	}
}
