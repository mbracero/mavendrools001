package org.mbracero.drools.config;

import java.util.List;

import org.drools.runtime.StatefulKnowledgeSession;
import org.drools.runtime.StatelessKnowledgeSession;
import org.mbracero.drools.vo.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component("DroolRule02b")
public class DroolRule02b {
	
	@Autowired
	@Qualifier("rule02bStateless")
	StatelessKnowledgeSession rule02bStateless;
	

	@Autowired
	@Qualifier("rule02bStateful")
	StatefulKnowledgeSession rule02bStateful;
	
	public void executeRulesStateless(List<Item> list) {
		rule02bStateless.execute(list);
	}
	
	public void executeRulesStateful(List<Item> list) {
		for (Item Item : list) {
			rule02bStateful.insert(Item);
		}
		rule02bStateful.fireAllRules();
	}
	
}
