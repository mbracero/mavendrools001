package org.mbracero.drools.config;

import java.util.List;

import org.drools.runtime.StatefulKnowledgeSession;
import org.drools.runtime.StatelessKnowledgeSession;
import org.mbracero.drools.vo.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component("DroolRule02a")
public class DroolRule02a {
	
	@Autowired
	@Qualifier("rule02aStateless")
	StatelessKnowledgeSession rule02aStateless;
	

	@Autowired
	@Qualifier("rule02aStateful")
	StatefulKnowledgeSession rule02aStateful;
	
	public void executeRulesStateless(List<Item> list) {
		rule02aStateless.execute(list);
	}
	
	public void executeRulesStateful(List<Item> list) {
		for (Item Item : list) {
			rule02aStateful.insert(Item);
		}
		rule02aStateful.fireAllRules();
	}
	
}
