package org.mbracero.drools.config;

import java.util.List;

import org.drools.runtime.StatefulKnowledgeSession;
import org.drools.runtime.StatelessKnowledgeSession;
import org.mbracero.drools.vo.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component("DroolRule01")
public class DroolRule01 {
	
	@Autowired
	@Qualifier("rule01Stateless")
	StatelessKnowledgeSession rule01Stateless;
	

	@Autowired
	@Qualifier("rule01Stateful")
	StatefulKnowledgeSession rule01Stateful;
	
	public void executeRulesStateless(List<Item> list) {
		rule01Stateless.execute(list);
	}
	
	public void executeRulesStateful(List<Item> list) {
		for (Item Item : list) {
			rule01Stateful.insert(Item);
		}
		rule01Stateful.fireAllRules();
	}
	
}
