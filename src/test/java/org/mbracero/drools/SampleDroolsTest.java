package org.mbracero.drools;

import static org.junit.Assert.assertEquals;

import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderError;
import org.drools.builder.KnowledgeBuilderErrors;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.io.ResourceFactory;
import org.drools.runtime.StatefulKnowledgeSession;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mbracero.drools.vo.SampleVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:drools-context.xml" })
public class SampleDroolsTest {

	@Autowired
	private KnowledgeBase kbase;

	@Before
	public void setup() {
		KnowledgeBuilder kbuilder = KnowledgeBuilderFactory
				.newKnowledgeBuilder();
		kbuilder.add(ResourceFactory.newClassPathResource("rules/sample.drl"),
				ResourceType.DRL);
		KnowledgeBuilderErrors errors = kbuilder.getErrors();
		if (errors.size() > 0) {
			for (KnowledgeBuilderError error : errors) {
				System.err.println(error);
			}
			throw new IllegalArgumentException("Could not parse knowledge.");
		}
		kbase = KnowledgeBaseFactory.newKnowledgeBase();
		kbase.addKnowledgePackages(kbuilder.getKnowledgePackages());

	}

	@Test
	public void testBasic() {

		StatefulKnowledgeSession ksession = kbase.newStatefulKnowledgeSession();

		SampleVO vo = new SampleVO();
		vo.setStringValue("Learning to drool");
		vo.setBooleanValue(true);
		ksession.insert(vo);
		ksession.fireAllRules();
		for (Object o : ksession.getObjects()) {
			if (o instanceof SampleVO) {
				assertEquals("Done.", ((SampleVO) o).getStringValue());
			}
		}

	}
}
