package org.mbracero.drools;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mbracero.drools.config.DroolRule02a;
import org.mbracero.drools.config.DroolRule02b;
import org.mbracero.drools.vo.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext.xml" })
public class DroolRule02Test {
	
	private final Logger log = Logger.getLogger(this.getClass());
	
	@SuppressWarnings("serial")
	private List<Item> samples = new CopyOnWriteArrayList<Item>() {{
		add(new Item(1, "Learning to drool", false));
		add(new Item(2, "Learning to drool 2", true));
		add(new Item(3, "Learning to drool", true));
		add(new Item(4, "Learning to drool", false));
		add(new Item(5, "", false));
		add(new Item(6, "Learning to drool", true));
		add(new Item(7, "Learning to drool", true));
	}};
	
	@Autowired
    @Qualifier("DroolRule02a")
    private DroolRule02a droolRule02a;

	@Autowired
    @Qualifier("DroolRule02b")
    private DroolRule02b droolRule02b;
	
	@Test
	public void testBasicStateless() {
		log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> STATELESS <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		log.info("Init :: " + samples);
		droolRule02a.executeRulesStateless(samples);
		droolRule02b.executeRulesStateless(samples);
		log.info("End :: " + samples);
	}
	
	@Test
	public void testBasicStateful() {
		log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> STATEFUL <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");
		log.info("Init :: " + samples);
		droolRule02a.executeRulesStateful(samples);
		droolRule02b.executeRulesStateful(samples);
		log.info("End :: " + samples);
	}
}
