## mavenDrools001

**Spring4 + maven + jboss drools + log4j**

Pruebas con jboss drools (rules engine)

Log mediante log4j (ver log4j.xml)

**Generar proyecto**

`mvn archetype:generate -DgroupId=org.mbracero -DartifactId=mavenDrools001 -DarchetypeArtifactId=maven-archetype-webapp -DinteractiveMode=false`


Existen TEST JUnit para pruebas.

(See: https://github.com/skprasadu/drools-spring-maven-samples)

